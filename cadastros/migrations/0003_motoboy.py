# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cadastros', '0002_auto_20151217_0346'),
    ]

    operations = [
        migrations.CreateModel(
            name='MotoBoy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='criado em')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='modificado em')),
                ('gender', models.CharField(verbose_name='gênero', max_length=1, choices=[('M', 'masculino'), ('F', 'feminino')])),
                ('cpf', models.CharField(verbose_name='CPF', max_length=11)),
                ('firstname', models.CharField(verbose_name='Nome', max_length=20)),
                ('lastname', models.CharField(verbose_name='Sobrenome', max_length=20)),
                ('email', models.EmailField(unique=True, verbose_name='e-mail', max_length=254)),
                ('phone', models.CharField(verbose_name='Fone', max_length=18)),
                ('birthday', models.DateTimeField(verbose_name='Nascimento')),
                ('data_admissao', models.DateField(blank=True, null=True)),
                ('data_insercao', models.DateField(auto_now_add=True, null=True)),
                ('rota', models.ForeignKey(verbose_name='Rota', to='cadastros.Rota')),
            ],
            options={
                'abstract': False,
                'ordering': ['firstname'],
            },
        ),
    ]
