# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cadastros', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='motoboy',
            name='pessoa',
        ),
        migrations.RemoveField(
            model_name='motoboy',
            name='rota',
        ),
        migrations.RemoveField(
            model_name='pessoa',
            name='estado',
        ),
        migrations.RemoveField(
            model_name='pessoa',
            name='estado_civil',
        ),
        migrations.DeleteModel(
            name='Produto',
        ),
        migrations.AlterField(
            model_name='rota',
            name='numero',
            field=models.IntegerField(verbose_name='Número da Rota'),
        ),
        migrations.DeleteModel(
            name='MotoBoy',
        ),
        migrations.DeleteModel(
            name='Pessoa',
        ),
    ]
