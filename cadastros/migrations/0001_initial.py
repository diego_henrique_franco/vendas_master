# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Doenca',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=60)),
                ('ativo', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uf', models.CharField(max_length=2)),
                ('nome', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='EstadoCivil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=60)),
                ('ativo', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name_plural': 'Formas de Pagamentos',
                'verbose_name': 'Forma de Pagamento',
            },
        ),
        migrations.CreateModel(
            name='FormaDePagamento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=60)),
                ('descricao', models.TextField(blank=True, null=True)),
                ('ativo', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='MotoBoy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_admissao', models.DateField(null=True)),
                ('data_insercao', models.DateField(null=True, auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=60)),
                ('email', models.EmailField(max_length=60)),
                ('data_nascimento', models.DateField()),
                ('cpf', models.CharField(max_length=11)),
                ('profissao', models.CharField(max_length=60)),
                ('endereco', models.CharField(null=True, max_length=255)),
                ('numero', models.CharField(null=True, max_length=10)),
                ('bairro', models.CharField(null=True, max_length=60)),
                ('complemento', models.CharField(blank=True, null=True, max_length=100)),
                ('cidade', models.CharField(null=True, max_length=60)),
                ('cep', models.CharField(null=True, max_length=8)),
                ('telefone', models.CharField(blank=True, null=True, max_length=10)),
                ('celular', models.CharField(null=True, max_length=10)),
                ('estado', models.ForeignKey(to='cadastros.Estado', null=True)),
                ('estado_civil', models.ForeignKey(to='cadastros.EstadoCivil')),
            ],
        ),
        migrations.CreateModel(
            name='Produto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=60)),
                ('ativo', models.BooleanField(default=True)),
                ('valor_prod', models.FloatField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Rota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.IntegerField(verbose_name='Nmero da Rota')),
                ('nome', models.CharField(max_length=60, verbose_name='Nome da Rota')),
            ],
        ),
        migrations.AddField(
            model_name='motoboy',
            name='pessoa',
            field=models.ForeignKey(to='cadastros.Pessoa', verbose_name='Nome da Pessoa', null=True),
        ),
        migrations.AddField(
            model_name='motoboy',
            name='rota',
            field=models.ForeignKey(to='cadastros.Rota', verbose_name='Rota'),
        ),
    ]
