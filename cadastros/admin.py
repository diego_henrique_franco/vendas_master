from django.contrib import admin
from .models import EstadoCivil, Doenca, Estado, MotoBoy, Rota, AnamneseFuncional


class MuralBoyAdmin(admin.ModelAdmin):
    list_display = ('recado', 'data_mural')
    list_display_links = ('recado', 'data_mural')
    search_fields = ('recado', 'data_mural')



# Register your models here.
admin.site.register(EstadoCivil)
admin.site.register(Doenca)
admin.site.register(Estado)
admin.site.register(MotoBoy)
admin.site.register(Rota)
admin.site.register(AnamneseFuncional)
