from __future__ import unicode_literals

from django.db.models import Sum, F
from django.utils.translation import ugettext_lazy as _
from django.db import models
from core.models import Person, Customer



# Create your models here.
class FormaDePagamento(models.Model):

    nome = models.CharField(max_length=60)
    descricao = models.TextField(blank=True, null=True)
    ativo = models.BooleanField(default=True)


class EstadoCivil(models.Model):
    nome = models.CharField(max_length=60)
    ativo = models.BooleanField(default=True)

    class Meta:
        verbose_name = u'Forma de Pagamento'
        verbose_name_plural = u'Formas de Pagamentos'


class Doenca(models.Model):
    nome = models.CharField(max_length=60)
    ativo = models.BooleanField(default=True)

#
# class Pessoa(models.Model):
#     nome = models.CharField(max_length=60)
#     email = models.EmailField(max_length=60)
#     data_nascimento = models.DateField()
#     cpf = models.CharField(max_length=11)
#     estado_civil = models.ForeignKey('EstadoCivil')
#     profissao = models.CharField(max_length=60)
#     endereco = models.CharField(max_length=255, null=True)
#     numero = models.CharField(max_length=10, null=True)
#     bairro = models.CharField(max_length=60, null=True)
#     complemento = models.CharField(max_length=100, blank=True, null=True)
#     cidade = models.CharField(max_length=60, null=True)
#     estado = models.ForeignKey('Estado', null=True)
#     cep = models.CharField(max_length=8, null=True)
#     telefone = models.CharField(max_length=10, null=True, blank=True)
#     celular = models.CharField(max_length=10, null=True)

class Estado(models.Model):
    uf = models.CharField(max_length=2)
    nome = models.CharField(max_length=60)


class Rota(models.Model):
    numero = models.IntegerField(_('Número da Rota'))
    nome = models.CharField(_('Nome da Rota'), max_length=60,)


class MotoBoy(Person):
    rota = models.ForeignKey(Rota, verbose_name=_('Rota'))
    data_admissao = models.DateField(null=True, blank=True)
    data_insercao = models.DateField(null=True, auto_now_add=True)


# class Produto(models.Model):
#     nome = models.CharField(max_length=60)
#     ativo = models.BooleanField(default=True)
#     valor_prod = models.FloatField(null=True)


class AnamneseFuncional(models.Model):
    pessoa = models.ForeignKey(Customer, verbose_name=_('Nome do Paciente'), null=True)
    motivo_consulta = models.CharField(max_length=60, null=True)
    meta = models.CharField(max_length=60, null=True)
    objetivo = models.CharField(max_length=60, null=True)
    como_conheceu = models.CharField(max_length=100, null=True, verbose_name=_('Como Conheceu a Nutrifuncional?'))
    fumante = models.BooleanField(default=False)
    consome_alcool = models.BooleanField(default=False)
    atividade_fisica = models.BooleanField(default=False)
    atividade_fisica_frequencia = models.CharField(max_length=60, blank=True, null=True)
    atividade_fisica_parado = models.CharField(max_length=60, blank=True, null=True)
    medicacao = models.TextField(max_length=60, blank=True, null=True)
    evacua_frequencia = models.IntegerField(null=True, verbose_name=u'Frequencia que vai ao banheiro:')
    evacua_remedio_bool = models.BooleanField(default=False, verbose_name=u'Usa medicamento para ir ao banheiro?')
    evacua_remedio_desc = models.CharField(max_length=100, blank=True, null=True,
                                           verbose_name=u'Qual medicamento utiliza?')
    doencas = models.ManyToManyField(Doenca, blank=True, verbose_name=u'Apresenta algum dos problemas abaixo?')
    peso = models.CharField(max_length=10, null=True)
    altura = models.CharField(max_length=30, null=True)
    refeicao_liquido = models.BooleanField(default=False, verbose_name=u'Toma Líquido junto as refeições?')
    refeicao_tv = models.BooleanField(default=False, verbose_name=u'Se alimenta em frente a TV?')
    problemas_digestao = models.BooleanField(default=False, verbose_name=u'Problemas de digestão?')
    alimentos_preferencia = models.TextField(max_length=300, blank=True, null=True,
                                             verbose_name=u'Alimentos de Preferência:')
    alimentos_naogosta = models.TextField(max_length=300, blank=True, null=True,
                                          verbose_name=u'Alimentos que não gosta:')
    alimentos_integrais = models.BooleanField(default=False, verbose_name=u'Consome alimentos integrais?')
    observacoes = models.TextField(blank=True, null=True, verbose_name=u'Observações:')
    desjejum_horario = models.TimeField(null=True, blank=True)
    desjejum_alimento = models.CharField(max_length=255, null=True, blank=True)
    lanche_manha_horario = models.TimeField(null=True, blank=True)
    lanche_manha_alimento = models.CharField(max_length=255, null=True, blank=True)
    almoco_horario = models.TimeField(null=True, blank=True)
    almoco_alimento = models.CharField(max_length=255, null=True, blank=True)
    sobremesa_horario = models.TimeField(null=True, blank=True)
    sobremesa_alimento = models.CharField(max_length=255, null=True, blank=True)
    lanche_tarde_horario = models.TimeField(null=True, blank=True)
    lanche_tarde_alimento = models.CharField(max_length=255, null=True, blank=True)
    jantar_horario = models.TimeField(null=True, blank=True)
    jantar_alimento = models.CharField(max_length=255, null=True, blank=True)
    ceia_horario = models.TimeField(null=True, blank=True)
    ceia_alimento = models.CharField(max_length=255, null=True, blank=True)

    class Meta:

        verbose_name = u'Anamnese Funcional'
        verbose_name_plural = u'Anamneses Funcionais'